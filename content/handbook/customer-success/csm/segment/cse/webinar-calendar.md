---

title: "CSM/CSE Webinar & Hands-On Labs Calendar"
aliases:
- /handbook/customer-success/csm/segment/cse/webinar-calendar/
- /handbook/customer-success/csm/segment/scale/webinar-calendar/
---
# On this page


View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---
# Upcoming Events

We’d like to invite you to our free upcoming webinars and labs in the month of April 2024. For May events, please check out our new webinar and labs [registration page here](https://university.gitlab.com/pages/gitlab-user-webinars)!

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## April 2024

### AMER Time Zone Webinars & Labs

#### Hands-On Advanced GitLab CI Lab 
##### April 24th, 2024 at 9:00-11:00AM PT / 12:00-2:00PM ET

Join us for a hands-on lab where we will go into advanced GitLab CI capabilities that allow customers to simplify pipeline yml code and optimize the execution time of pipelines.

We will cover:
- Storing build resources in GitLab registries
- Managing artifacts, dependencies, and environment variables between jobs
- Running a job multiple times in parallel with different variables
- Triggering downstream pipelines

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_D_pFLbsvRS-bLD0p7ut24g#/registration)

#### AI in DevSecOps - GitLab Webinar
##### April 30th, 2024 at 9:00-10:00AM Pacific Time / 12:00-1:00PM Eastern Time

Join us for the AI in DevSecOps webinar where we will explore Artificial Intelligence and how it fits within the DevSecOps lifecycle. In this session, we will cover foundational implementation and use case scenarios such as Code Suggestions and Vulnerability Explanations.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_-kIPWEcnTjOzVrKplN126g#/registration)

### EMEA Time Zone Webinars & Labs

#### Hands-On Advanced GitLab CI Lab 
##### April 24th, 2024 at 9:00-11:00AM UTC / 11:00AM-1:00PM CET

Join us for a hands-on lab where we will go into advanced GitLab CI capabilities that allow customers to simplify pipeline yml code and optimize the execution time of pipelines.

We will cover:
- Storing build resources in GitLab registries
- Managing artifacts, dependencies, and environment variables between jobs
- Running a job multiple times in parallel with different variables
- Triggering downstream pipelines

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_OQNV9scJSIaelQkXl5mbnw#/registration)

#### AI in DevSecOps - GitLab Webinar
##### April 30th, 2024 at at 9:00-10:00AM UTC / 11:00AM-12:00PM CET

Join us for the AI in DevSecOps webinar where we will explore Artificial Intelligence and how it fits within the DevSecOps lifecycle. In this session, we will cover foundational implementation and use case scenarios such as Code Suggestions and Vulnerability Explanations.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN__PRkLukaTyqA1uwpoe9OwA#/registration)

## May 2024

Please check out our new webinar and labs event [registration page here](https://university.gitlab.com/pages/gitlab-user-webinars). We are looking forward to hosting you for some - or all - of our May events!
